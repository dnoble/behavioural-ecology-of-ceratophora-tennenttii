
## Sexual and natural selection on colour signals in Sri-Lankan Horned lizards

This repo allows reproducibility of all analyses, tables and figures in the following paper:

Martin J. Whiting, Daniel W.A. Noble and Richura Somerweera. Title here. Journal Here, etc.

This paper is not yet published. 

To reproduce all analyses Rscript("Analysis/analysisALL-00.R") from the folder directory. This can also be acheived by running source("./Analysis/analysisALL-00.R") in R. Model checking dignostic plots will be produced when analysis is run. 

For issues contact Dan Noble: daniel.noble@mq.edu.au




